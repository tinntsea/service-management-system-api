create table if not exists service (
    id integer auto_increment primary key,
    name varchar(255) not null,
    price DECIMAL(10,2) not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

create table if not exists customer (
    id integer auto_increment primary key,
    full_name varchar(255) not null,
    phone_number varchar(12) not null,
#     email varchar(255),
    created_at timestamp not null default now(),
    updated_at timestamp not null default now()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

create table if not exists employee (
    id integer auto_increment primary key,
    full_name varchar(255) not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

create table if not exists appointment (
    id integer auto_increment primary key,
    service_id integer not null REFERENCES service(id),
    customer_id integer not null REFERENCES customer(id),
    employee_id integer not null REFERENCES employee(id),
    from_time timestamp not null,
    to_time timestamp not null,
    status varchar(20) not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

create table if not exists invoice (
   id integer auto_increment primary key,
   appointment_id integer not null REFERENCES appointment(id),
   amount DECIMAL(10,2) not null,
   created_at timestamp not null default now(),
   updated_at timestamp not null default now()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
