## Technical Stack:
- Java
- Spring Boot Framework
- Docker and docker-compose
- Mysql

## Development Guidelines
1. Install docker at https://docs.docker.com/get-docker/ and docker-compose at https://docs.docker.com/compose/install/

2. Run the mysql database
```
docker-compose up -d
```

Check if it's running
```
docker ps
```

The result should be like this
```
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS                    PORTS                                                  NAMES
7d89a07fd67b   mysql:8.0.26   "docker-entrypoint.s…"   39 minutes ago   Up 39 minutes (healthy)   33060/tcp, 0.0.0.0:3206->3306/tcp, :::3206->3306/tcp   sms_mysql_db_1
```

Mysql information:
```
Host: localhost
Port: 3206
Database: service_mgt
Username: service_mgt_user
Password: service_mgt_password
```

3. Run the application
We are using Java and Spring Boot framework for API. You should read the following documents before starting the development:
   - https://spring.io/guides/gs/accessing-data-mysql/

Recommended IDE: IntelliJ

4. Testing
4.1 Create new service, customer, employee, appointment
```
curl -X POST http://localhost:8080/api/services -H 'Content-Type: application/json' -d '{"name":"Haircut","price":5.0}'
curl -X POST http://localhost:8080/api/customers -H 'Content-Type: application/json' -d '{"name":"Tin Nguyen","phone":"1234567"}'
curl -X POST http://localhost:8080/api/employees -H 'Content-Type: application/json' -d '{"name":"Tuan"}'
curl -X POST http://localhost:8080/api/appointments -H 'Content-Type: application/json' -d '{"serviceId":1,"customerId":2,"employeeId":3}'
```

4.2 Get all
```
curl -X GET http://localhost:8080/api/services 
curl -X GET http://localhost:8080/api/customers 
curl -X GET http://localhost:8080/api/employees 
curl -X GET http://localhost:8080/api/appointments
```