package com.bordeaux.sms.repository;

import com.bordeaux.sms.entity.Customer;
import com.bordeaux.sms.entity.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    @Query("SELECT e FROM Employee e order by e.updatedAt desc")
    Iterable<Employee> findEmployees();
}
