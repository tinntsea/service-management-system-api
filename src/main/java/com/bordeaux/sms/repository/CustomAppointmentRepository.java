package com.bordeaux.sms.repository;

import com.bordeaux.sms.entity.Appointment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomAppointmentRepository {
    public List<Appointment> getAppointments(String status);
}
