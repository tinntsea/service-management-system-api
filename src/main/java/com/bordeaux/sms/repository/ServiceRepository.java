package com.bordeaux.sms.repository;

import com.bordeaux.sms.entity.Employee;
import com.bordeaux.sms.entity.Service;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ServiceRepository extends CrudRepository<Service, Integer> {
    @Query("SELECT s FROM Service s order by s.updatedAt desc")
    Iterable<Service> findServices();
}
