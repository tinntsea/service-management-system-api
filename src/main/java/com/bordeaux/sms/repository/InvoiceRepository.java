package com.bordeaux.sms.repository;

import com.bordeaux.sms.entity.Invoice;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceRepository extends CrudRepository<Invoice, Integer> {

}
