package com.bordeaux.sms.repository;

import com.bordeaux.sms.entity.Appointment;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CustomAppointmentRepositoryImpl implements CustomAppointmentRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Appointment> getAppointments(String status) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Appointment> query = cb.createQuery(Appointment.class);
        Root<Appointment> appointmentRoot = query.from(Appointment.class);
//        query.select(cb.construct(Appointment.class, root.get(Author_.firstName), root.get(Author_.lastName)))
//                .where(cb.equal(root.get(Author_.firstName), firstName));
//
//        return entityManager.createQuery(query).getResultList();

        query.select(appointmentRoot)
//                .where(cb.or(predicates.toArray(new Predicate[predicates.size()])))
//        .orderBy()
        ;

        return entityManager.createQuery(query)
                .getResultList();
    }
}
