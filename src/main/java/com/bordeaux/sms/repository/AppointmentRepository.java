package com.bordeaux.sms.repository;

import com.bordeaux.sms.entity.Appointment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AppointmentRepository extends CrudRepository<Appointment, Integer> {
    @Query("SELECT a FROM Appointment a WHERE a.status = ?1 order by a.updatedAt desc")
    Iterable<Appointment> findAppointmentsByStatus(String status);

    @Query("SELECT a FROM Appointment a order by a.updatedAt desc")
    Iterable<Appointment> findAppointments();
}
