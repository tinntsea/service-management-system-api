package com.bordeaux.sms.repository;

import com.bordeaux.sms.entity.Appointment;
import com.bordeaux.sms.entity.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    @Query("SELECT c FROM Customer c order by c.updatedAt desc")
    Iterable<Customer> findCustomers();
}
