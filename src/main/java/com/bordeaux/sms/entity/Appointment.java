package com.bordeaux.sms.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Appointment {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    private Service service;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Employee employee;

    private LocalDateTime fromTime;

    private LocalDateTime toTime;

    private String status = STATUS_NEW;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public static String STATUS_NEW = "new";
    public static String STATUS_DONE = "done";

    public Appointment() {}

    public Appointment(Service service, Customer customer, Employee employee, LocalDateTime fromTime, LocalDateTime toTime) {
        this.service = service;
        this.customer = customer;
        this.employee = employee;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }

    public Integer getId() {
        return id;
    }

    public Service getService() {
        return service;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public LocalDateTime getFromTime() {
        return fromTime;
    }

    public LocalDateTime getToTime() {
        return toTime;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setFromTime(LocalDateTime fromTime) {
        this.fromTime = fromTime;
    }

    public void setToTime(LocalDateTime toTime) {
        this.toTime = toTime;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
