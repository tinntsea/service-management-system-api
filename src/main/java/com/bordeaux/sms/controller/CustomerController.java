package com.bordeaux.sms.controller;

import com.bordeaux.sms.entity.Customer;
import com.bordeaux.sms.form.CustomerForm;
import com.bordeaux.sms.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping(path="/api")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @PostMapping(path="/customers", consumes = "application/json", produces = "application/json")
    public @ResponseBody Customer addCustomer (
            @RequestBody CustomerForm customerForm
    ) {
        Customer customer = new Customer(customerForm.getName(), customerForm.getPhone());
        return customerRepository.save(customer);
    }

    @GetMapping(path="/customers", produces = "application/json")
    public @ResponseBody Iterable<Customer> getAllCustomers() {
        return customerRepository.findCustomers();
    }

    @PutMapping(path = "/customers/{id}", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Customer editCustomer(@RequestBody CustomerForm customerForm, @PathVariable Integer id) {
        return customerRepository.findById(id)
                .map(customer -> {
                    customer.setPhoneNumber(customerForm.getPhone());
                    customer.setFullName(customerForm.getName());
                    customer.setUpdatedAt(LocalDateTime.now());
                    return customerRepository.save(customer);
                })
                .orElseThrow(() -> new RuntimeException(""));
    }

    @PutMapping(path="/customers", consumes = "application/json", produces = "application/json")
    public @ResponseBody Customer amendCustomer (
            @RequestBody CustomerForm customerForm
    ) {
        return customerRepository.findById(customerForm.getId())
                .map(customer -> {
                    customer.setPhoneNumber(customerForm.getPhone());
                    customer.setFullName(customerForm.getName());
                    customer.setUpdatedAt(LocalDateTime.now());
                    return customerRepository.save(customer);
                })
                .orElseThrow(() -> new RuntimeException(""));
    }

    @DeleteMapping(path="/customers", consumes = "application/json", produces = "application/json")
    public @ResponseBody Iterable<Customer> deleteCustomer(@RequestBody CustomerForm customerForm) {
        customerRepository.deleteById(customerForm.getId());
        return customerRepository.findAll();
    }

    // @TODO: remove
    @PostMapping(path="/customer/add", consumes = "application/json", produces = "application/json")
    public @ResponseBody Customer addCustomerOld (
            @RequestBody CustomerForm customerForm
    ) {
        Customer customer = new Customer(customerForm.getName(), customerForm.getPhone());
        return customerRepository.save(customer);
    }

    // @TODO: remove
    @GetMapping(path="/customer/all")
    public @ResponseBody Iterable<Customer> getAllCustomersOld() {
        return customerRepository.findAll();
    }

}

