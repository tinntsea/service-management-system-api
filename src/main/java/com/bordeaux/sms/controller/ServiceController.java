package com.bordeaux.sms.controller;

import com.bordeaux.sms.entity.Service;
import com.bordeaux.sms.form.ServiceForm;
import com.bordeaux.sms.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping(path = "/api")
public class ServiceController {

    @Autowired
    private ServiceRepository serviceRepository;

    @PostMapping(path = "/services", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Service addService(@RequestBody ServiceForm serviceForm) {
        Service service = new Service(serviceForm.getName(), serviceForm.getPrice());
        return serviceRepository.save(service);
    }

    @GetMapping(path = "/services", produces = "application/json")
    public @ResponseBody
    Iterable<Service> getAllServices() {
        return serviceRepository.findServices();
    }

    @PutMapping(path = "/services/{id}", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Service amendService(@RequestBody ServiceForm serviceForm, @PathVariable Integer id) {
        return serviceRepository.findById(id)
                .map(service -> {
                    service.setName(serviceForm.getName());
                    service.setPrice(serviceForm.getPrice());
                    service.setUpdatedAt(LocalDateTime.now());
                    return serviceRepository.save(service);
                })
                .orElseGet(() -> {
                    Service service = new Service(serviceForm.getName(), serviceForm.getPrice());
                    return serviceRepository.save(service);
                });
    }

    @PutMapping(path = "/services", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Service amendService(@RequestBody ServiceForm serviceForm) {
        return serviceRepository.findById(serviceForm.getId())
                .map(service -> {
                    service.setName(serviceForm.getName());
                    service.setPrice(serviceForm.getPrice());
                    service.setUpdatedAt(LocalDateTime.now());
                    return serviceRepository.save(service);
                })
                .orElseGet(() -> {
                    Service service = new Service(serviceForm.getName(), serviceForm.getPrice());
                    return serviceRepository.save(service);
                });
    }

    @DeleteMapping(path = "/services", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Iterable<Service> deleteService(@RequestBody ServiceForm serviceForm) {
        serviceRepository.deleteById(serviceForm.getId());
        return serviceRepository.findAll();
    }

    // @TODO: remove
    @PostMapping(path = "/service/add", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Service addServiceOld(
            @RequestBody ServiceForm serviceForm
    ) {
        Service service = new Service(serviceForm.getName(), serviceForm.getPrice());
        return serviceRepository.save(service);
    }

    // @TODO: remove
    @GetMapping(path = "/service/all")
    public @ResponseBody
    Iterable<Service> getAllServicesOld() {
        return serviceRepository.findAll();
    }

}

