package com.bordeaux.sms.controller;

import com.bordeaux.sms.entity.Appointment;
import com.bordeaux.sms.entity.Customer;
import com.bordeaux.sms.entity.Employee;
import com.bordeaux.sms.entity.Service;
import com.bordeaux.sms.form.AppointmentForm;
import com.bordeaux.sms.repository.AppointmentRepository;
import com.bordeaux.sms.repository.CustomerRepository;
import com.bordeaux.sms.repository.EmployeeRepository;
import com.bordeaux.sms.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping(path = "/api")
public class AppointmentController {

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @PostMapping(path = "/appointments", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Appointment addAppointment(@RequestBody AppointmentForm appointmentForm) {
        Service service = serviceRepository.findById(appointmentForm.getServiceId()).get();
        Customer customer = customerRepository.findById(appointmentForm.getCustomerId()).get();
        Employee employee = employeeRepository.findById(appointmentForm.getEmployeeId()).get();
        Long fromTime1 = (null != appointmentForm.getFromTime()) ? appointmentForm.getFromTime() : LocalDateTime.now().getSecond();
        LocalDateTime fromTime = (new Timestamp(fromTime1)).toLocalDateTime();
        Appointment appointment = new Appointment(service, customer, employee, fromTime, fromTime.plusHours(1));

        return appointmentRepository.save(appointment);
    }

    @PutMapping(path = "/appointments/{id}/complete", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Appointment doneAppointment(@PathVariable Integer id) {
        return appointmentRepository.findById(id)
                .map(appointment -> {
                    appointment.setStatus(Appointment.STATUS_DONE);

                    return appointmentRepository.save(appointment);
                })
                .orElseThrow(() -> new RuntimeException(""));
    }

    @GetMapping(path = "/appointments", produces = "application/json")
    public @ResponseBody
    Iterable<Appointment> getAllAppointments(@RequestParam(required = false, name = "status") String status) {
        if (null != status && !status.isEmpty()) {
            return appointmentRepository.findAppointmentsByStatus(status);
        }

        return appointmentRepository.findAppointments();
    }

    @PutMapping(path = "/appointments", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Appointment amendAppointment(@RequestBody AppointmentForm appointmentForm) {
        Service service = serviceRepository.findById(appointmentForm.getServiceId()).get();
        Customer customer = customerRepository.findById(appointmentForm.getCustomerId()).get();
        Employee employee = employeeRepository.findById(appointmentForm.getEmployeeId()).get();

        return appointmentRepository.findById(appointmentForm.getId())
                .map(appointment -> {
                    appointment.setService(service);
                    appointment.setCustomer(customer);
                    appointment.setEmployee(employee);
                    appointment.setUpdatedAt(LocalDateTime.now());
                    return appointmentRepository.save(appointment);
                })
                .orElseGet(() -> {
                    Appointment appointment = new Appointment(service, customer, employee, LocalDateTime.now(), LocalDateTime.now().plusHours(1));
                    return appointmentRepository.save(appointment);
                });
    }

    @DeleteMapping(path = "/appointments", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Iterable<Appointment> deleteAppointment(@RequestBody AppointmentForm appointmentForm) {
        appointmentRepository.deleteById(appointmentForm.getId());
        return appointmentRepository.findAll();
    }

    // Obsolete -> ???
    @PostMapping(path = "/appointment/add")
    public @ResponseBody
    Appointment addAppointmentOld(
            @RequestParam Integer serviceId,
            @RequestParam Integer customerId,
            @RequestParam Integer employeeId
    ) {
        Service service = serviceRepository.findById(serviceId).get();
        Customer customer = customerRepository.findById(customerId).get();
        Employee employee = employeeRepository.findById(employeeId).get();
        Appointment appointment = new Appointment(service, customer, employee, LocalDateTime.now(), LocalDateTime.now().plusHours(1));
        return appointmentRepository.save(appointment);
    }

    // Obsolete -> ???
    @GetMapping(path = "/appointment/all")
    public @ResponseBody
    Iterable<Appointment> getAllAppointmentsOld() {
        return appointmentRepository.findAll();
    }

}

