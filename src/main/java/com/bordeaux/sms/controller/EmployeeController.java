package com.bordeaux.sms.controller;

import com.bordeaux.sms.entity.Employee;
import com.bordeaux.sms.form.EmployeeForm;
import com.bordeaux.sms.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping(path = "/api")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @PostMapping(path = "/employees", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Employee addEmployee(@RequestBody EmployeeForm employeeForm) {
        Employee employee = new Employee(employeeForm.getName());
        return employeeRepository.save(employee);
    }

    @GetMapping(path = "/employees", produces = "application/json")
    public @ResponseBody
    Iterable<Employee> getAllEmployees() {
        return employeeRepository.findEmployees();
    }

    @PutMapping(path = "/employees/{id}", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Employee editCustomer(@RequestBody EmployeeForm employeeForm, @PathVariable Integer id) {
        return employeeRepository.findById(id)
                .map(employee -> {
                    employee.setFullName(employeeForm.getName());
                    employee.setUpdatedAt(LocalDateTime.now());
                    return employeeRepository.save(employee);
                })
                .orElseThrow(() -> new RuntimeException(""));
    }

    @PutMapping(path = "/employees", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Employee amendEmployee(@RequestBody EmployeeForm employeeForm) {
        return employeeRepository.findById(employeeForm.getId())
                .map(employee -> {
                    employee.setFullName(employeeForm.getName());
                    employee.setUpdatedAt(LocalDateTime.now());
                    return employeeRepository.save(employee);
                })
                .orElseThrow(() -> new RuntimeException(""));
    }

    @DeleteMapping(path = "/employees", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Iterable<Employee> deleteEmployee(@RequestBody EmployeeForm employeeForm) {
        employeeRepository.deleteById(employeeForm.getId());
        return employeeRepository.findAll();
    }

    // @TODO: remove
    @PostMapping(path = "/employee/add", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    Employee addEmployeeOld(@RequestBody EmployeeForm employeeForm) {
        Employee employee = new Employee(employeeForm.getName());
        return employeeRepository.save(employee);
    }

    // @TODO: remove
    @GetMapping(path = "/employee/all")
    public @ResponseBody
    Iterable<Employee> getAllEmployeesOld() {
        return employeeRepository.findAll();
    }

}

