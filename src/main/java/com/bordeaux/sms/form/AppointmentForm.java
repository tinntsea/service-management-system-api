package com.bordeaux.sms.form;

import java.time.LocalDateTime;

public class AppointmentForm {

    private Integer serviceId;

    private Integer customerId;

    private Integer employeeId;

    private Long fromTime;

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }
}
